//
//  CategoryViewController.swift
//  Repsio
//
//  Created by alexander on 6/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewController:UITableViewController, NSFetchedResultsControllerDelegate{
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        
        if let identifier = segue.identifier{
            if  identifier == "CategoryDetailSegue"{
                
                let next = segue.destinationViewController as UINavigationController
                
                let detail  = next.topViewController as CategoryDetailViewController
                
                let indexPath = self.tableView.indexPathForSelectedRow()
                
                var item: Category = fetchedResultsController.objectAtIndexPath(indexPath) as Category

                detail.category = item
            }
        }
 
        
    }
    //:MARK Getter
    @lazy
    var fetchedResultsController: NSFetchedResultsController =
    {
        var fetch: NSFetchRequest = NSFetchRequest(entityName: "Category")
        fetch.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        var frc: NSFetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetch,
            managedObjectContext: CategoryStore.sharedStore().managedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
        }()
    

    func performFetchAndReload(reload: Bool)
    {
        var moc = fetchedResultsController.managedObjectContext
        
        moc.performBlockAndWait {
            var error: NSErrorPointer = nil
            if !self.fetchedResultsController.performFetch(error) {
                println("\(error)")
            }
            
            if reload {
                self.tableView.reloadData()
            }
        }
    }
    
    func updateCell(cell: UITableViewCell, object: Category) {
        cell.textLabel.text = object.name
    }
    
    //:MARK UIViewController

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        title = "Categories"
        
        performFetchAndReload(false)
    }

    //:MARK UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView?) -> Int
    {
        return fetchedResultsController.sections.count
    }
    
    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int
    {
        let sectionInfo = fetchedResultsController.sections[section] as NSFetchedResultsSectionInfo
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell?
    {
        let cell = tableView!.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath) as UITableViewCell
        var item: Category = fetchedResultsController.objectAtIndexPath(indexPath) as Category
        
        updateCell(cell, object: item)
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let item = fetchedResultsController.objectAtIndexPath(indexPath) as Category
            CategoryStore.sharedStore().deleteObject(item)
        }
    }
    
    
    //:MARK UITableViewDelegate
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

    }
    
    //:MARK NSFetchedResultsController
    
    func controllerWillChangeContent(controller: NSFetchedResultsController!)
    {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeSection sectionInfo: NSFetchedResultsSectionInfo!, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType)
    {
        if type == NSFetchedResultsChangeInsert {
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
        }
        else if type == NSFetchedResultsChangeDelete {
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeObject anObject: AnyObject!, atIndexPath indexPath: NSIndexPath!, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath!)
    {
    
        switch type {
        case NSFetchedResultsChangeInsert:
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        case NSFetchedResultsChangeDelete:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        case NSFetchedResultsChangeUpdate:
            updateCell(tableView.cellForRowAtIndexPath(indexPath), object: anObject as Category)
        case NSFetchedResultsChangeMove:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        default:
            println("unhandled didChangeObject update type \(type)")
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController!)
    {
        tableView.endUpdates()
    }

}