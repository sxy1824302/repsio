//
//  CreateRecordController.swift
//  Repsio
//
//  Created by alexander on 6/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit
import Foundation


class CreateRecordController:UITableViewController{

    @IBOutlet var dateInfoLabel: UILabel
    
    @IBOutlet var numberTextField: UITextField
    
    let formatter:NSDateFormatter = NSDateFormatter()

    @IBOutlet var setsStepper: UIStepper
    
    @IBOutlet var setsLabel: UILabel
    
    @IBOutlet var setDuration: UITextField
    
    var category:Category!
    
    override func viewDidLoad() {
        
        numberTextField.text = "3"
        
        setDuration.text = "4"
        
        setsLabel.text = "\(setsStepper.value)"
        
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateInfoLabel.text = formatter.stringFromDate(NSDate())
    }
    
    @IBAction func setsChanged(sender: AnyObject) {
        
        let stepper = sender as UIStepper
        setsLabel.text = "\(stepper.value)"
    }
    @IBAction func done(sender: AnyObject) {
        
        if let repCount = numberTextField.text.toInt(){
  
            

            if let session = CategoryStore.sharedStore().createNewSessionForCategory( category!, andRepeatCount: numberTextField.text.toInt()!, andSetsCount: Int(setsStepper.value)){
   
                session.perSetDuration = setDuration.text.toInt()!
                
                dismissModalViewControllerAnimated(true)
            }
            
        }
    }
    
    
}