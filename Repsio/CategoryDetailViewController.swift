//
//  DetailViewController.swift
//  Repsio
//
//  Created by alexander on 6/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit
import CoreData
class CategoryDetailViewController: UITableViewController, UISplitViewControllerDelegate ,NSFetchedResultsControllerDelegate{

    @IBOutlet var detailDescriptionLabel: UILabel
    var masterPopoverController: UIPopoverController? = nil

    var category:Category!
    
    let dateFormatter:NSDateFormatter =
    {
        var formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        formatter.timeStyle = .ShortStyle
        return formatter
        }()
    
    //:MARK Getter
    @lazy
    var fetchedResultsController: NSFetchedResultsController =
    {
        var fetch: NSFetchRequest = NSFetchRequest(entityName: "Session")
        fetch.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: true)]
        
        var frc: NSFetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetch,
            managedObjectContext: CategoryStore.sharedStore().managedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
        }()
    
    
    func performFetchAndReload(reload: Bool)
    {
        var moc = fetchedResultsController.managedObjectContext
        
        moc.performBlockAndWait {
            var error: NSErrorPointer = nil
            if !self.fetchedResultsController.performFetch(error) {
                println("\(error)")
            }
            
            if reload {
                self.tableView.reloadData()
            }
        }
    }
    
    

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()

            if self.masterPopoverController != nil {
                self.masterPopoverController!.dismissPopoverAnimated(true)
            }
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.valueForKey("timeStamp").description
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if let identifier = segue.identifier{
            
            if identifier == "CreateRecordSegue"{
                
                let next = segue.destinationViewController as CreateRecordController
                
                next.category = category
            }else if identifier == "WorkSegue"{
                let index = self.tableView.indexPathForSelectedRow()
                
                let session = fetchedResultsController.objectAtIndexPath(index) as Session
                
                let workvc = segue.destinationViewController as WorkViewController
                
                workvc.session = session
            }
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
         performFetchAndReload(false)
        
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updateCell(cell: SessionCell, object: Session) {
     cell.textLabel.text = dateFormatter.stringFromDate(object.timeStamp)
        cell.detailTextLabel.text = "\(object.setInfos.count) sets \(object.repsCount) reps"
    }
    //:MARK UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView?) -> Int
    {
        return fetchedResultsController.sections.count
    }
    
    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int
    {
        let sectionInfo = fetchedResultsController.sections[section] as NSFetchedResultsSectionInfo
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell?
    {
        let cell = tableView!.dequeueReusableCellWithIdentifier("SessionCell", forIndexPath: indexPath) as SessionCell
        
        var item: Session = fetchedResultsController.objectAtIndexPath(indexPath) as Session
        
        updateCell(cell, object: item)
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let item = fetchedResultsController.objectAtIndexPath(indexPath) as Category
            CategoryStore.sharedStore().deleteObject(item)
        }
    }
    
    
    //:MARK UITableViewDelegate
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    //:MARK NSFetchedResultsController
    
    func controllerWillChangeContent(controller: NSFetchedResultsController!)
    {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeSection sectionInfo: NSFetchedResultsSectionInfo!, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType)
    {
        if type == NSFetchedResultsChangeInsert {
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
        }
        else if type == NSFetchedResultsChangeDelete {
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeObject anObject: AnyObject!, atIndexPath indexPath: NSIndexPath!, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath!)
    {
        
        switch type {
        case NSFetchedResultsChangeInsert:
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        case NSFetchedResultsChangeDelete:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        case NSFetchedResultsChangeUpdate:
            updateCell(tableView.cellForRowAtIndexPath(indexPath) as SessionCell, object: anObject as Session)
        case NSFetchedResultsChangeMove:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        default:
            println("unhandled didChangeObject update type \(type)")
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController!)
    {
        tableView.endUpdates()
    }

    
    // #pragma mark - Split view

    func splitViewController(splitController: UISplitViewController, willHideViewController viewController: UIViewController, withBarButtonItem barButtonItem: UIBarButtonItem, forPopoverController popoverController: UIPopoverController) {
        barButtonItem.title = "Master" // NSLocalizedString(@"Master", @"Master")
        self.navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
        self.masterPopoverController = popoverController
    }

    func splitViewController(splitController: UISplitViewController, willShowViewController viewController: UIViewController, invalidatingBarButtonItem barButtonItem: UIBarButtonItem) {
        // Called when the view is shown again in the split view, invalidating the button and popover controller.
        self.navigationItem.setLeftBarButtonItem(nil, animated: true)
        self.masterPopoverController = nil
    }
    func splitViewController(splitController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return true
    }
    




}


class SessionCell:UITableViewCell{
    

}


