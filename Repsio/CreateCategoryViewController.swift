//
//  CreateCategoryViewController.swift
//  Repsio
//
//  Created by alexander on 6/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit


class CreateCategoryViewController:UIViewController,UITextFieldDelegate{
    
    var categoryName = "New Item"
    
    
    @IBOutlet var textField: UITextField
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool{
        
        categoryName = textField.text
        
        return true
    }
    
    override func viewDidLoad() {
        
        textField.text = categoryName
    }
    
    
    @IBAction func create(sender: AnyObject) {
        
        
        if let category = CategoryStore.sharedStore().createNewCategoryWithName(categoryName){
             dismissModalViewControllerAnimated(true)
        }
       
    }
    

    @IBAction func cancel(sender: AnyObject) {
        
        dismissModalViewControllerAnimated(true)
    }
}