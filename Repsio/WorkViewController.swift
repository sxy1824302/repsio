//
//  WorkViewController.swift
//  Repsio
//
//  Created by alexander on 7/3/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit

enum WorkingState{
    case Paused
    case Runinng
    case Initialized
    
}
extension Int {
    func format(f: String) -> String {
        return NSString(format: "%\(f)d", self)
    }
}

extension Double {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self)
    }
}

class WorkViewController:UIViewController{
    
    var state:WorkingState = WorkingState.Initialized

    @IBOutlet var startButton: UIBarButtonItem
    
    @IBOutlet var pauseButton: UIBarButtonItem
    @IBOutlet var resumeButton: UIBarButtonItem
    
    @IBOutlet var targetTimeLabel: UILabel
    
    var timer:NSTimer?
    
    var session:Session!
    
    var currentInterval = 0
    
    var activeSetInfo:SetInfo?
    {
        let index = session.activeSetIndex
            if index.integerValue >= session.setInfos.count{
                return nil
            }
            
       return session.setInfos.objectAtIndex(index.integerValue) as? SetInfo
    }
    
    

    @IBOutlet var totalSetsLabel: UILabel
    
    @IBOutlet var countDownLabel: UILabel
    @IBOutlet var currentSetLabel: UILabel
    override func viewDidLoad() {
        
        totalSetsLabel.text = "\(session!.setInfos.count) sets"
        
        currentSetLabel.text = "Set \(session.activeSetIndex.integerValue+1)"
        
        pauseButton.enabled = false
        resumeButton.enabled = false
        
        
        let (m,s) = converToMS(session.perSetDuration.integerValue*60)
        let format = ".2"
        targetTimeLabel.text =  "\(m.format(format)) : \(s.format(format))"
        update(nil)
    }
    
    func converToMS(interval:Int)->(minute:Int,seconds:Int){
        
        let minute = interval/60
        
        let second = interval%60

        return (minute,second)
    }
    
    func update(timer:NSTimer?){
        currentInterval++
        
        let (m,s) = converToMS(currentInterval)
        let format = ".2"
        countDownLabel.text =  "\(m.format(format)) : \(s.format(format))"
    }
   
    @IBAction func action(sender: UIBarButtonItem) {
        
        if sender == startButton{
            startButton.enabled = false
            resumeButton.enabled = false
            pauseButton.enabled = true
            timer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "update:", userInfo: nil, repeats: true)
        }else if sender == resumeButton{
            startButton.enabled = false
            resumeButton.enabled = false
            pauseButton.enabled = true
        
            timer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "update:", userInfo: nil, repeats: true)
        }else{
            pauseButton.enabled = false
            resumeButton.enabled = true
            timer!.invalidate()
            timer = nil
        }
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let aTimer = timer{
            aTimer.invalidate()
        }
        
        CategoryStore.sharedStore().save()
    }

}
