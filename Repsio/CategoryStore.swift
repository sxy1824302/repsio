//
//  CategoryStore.swift
//  Repsio
//
//  Created by alexander on 6/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

import UIKit
import CoreData
import Foundation
class SetInfo: NSManagedObject {
    @NSManaged var duration:NSNumber
    @NSManaged var finishedRepsCount:NSNumber

    override func awakeFromInsert() {
        super.awakeFromInsert()

        self.duration = 0
        self.finishedRepsCount = 0
    }

}

class Session: NSManagedObject {
    @NSManaged var activeSetIndex:NSNumber
    @NSManaged var repsCount: NSNumber
    @NSManaged var timeStamp: NSDate
    @NSManaged var category: Category
    @NSManaged var perSetDuration:NSNumber
    @NSManaged var setInfos:NSOrderedSet
    
    override func awakeFromInsert() {
        super.awakeFromInsert()
        self.timeStamp = NSDate()
        self.repsCount = 0
    }
    
    
    //Working with sets
    func addSetInfosObject(employee: SetInfo?){
        self.mutableOrderedSetValueForKey("setInfos").addObject(employee)
    }
    
    func removeSetInfosObject(employee: SetInfo?){
        self.mutableOrderedSetValueForKey("setInfos").removeObject(employee)
    }
    
    func addSetInfos(records: NSSet?){

        self.willChangeValueForKey("setInfos", withSetMutation: NSKeyValueSetMutationKind.UnionSetMutation, usingObjects: records)
        self.mutableOrderedSetValueForKey("setInfos").unionSet(records)
        self.didChangeValueForKey("setInfos", withSetMutation: NSKeyValueSetMutationKind.UnionSetMutation, usingObjects: records)
    }
    
    func removeSetInfos(records: NSSet?){
        self.willChangeValueForKey("setInfos", withSetMutation: NSKeyValueSetMutationKind.MinusSetMutation, usingObjects: records)
        self.mutableOrderedSetValueForKey("setInfos").minusSet(records)
        self.didChangeValueForKey("setInfos", withSetMutation: NSKeyValueSetMutationKind.MinusSetMutation, usingObjects: records)
    }
}

class Category: NSManagedObject {
    
    @NSManaged var name: String
    
    @NSManaged var records: NSSet
    
    
    //Working with Employees
    func addRecordObject(employee: Session?){
        let set:NSSet = NSSet(object: employee)
        self.addRecords(set)
    }
    
    func removeRecordObject(employee: Session?){
        let set:NSSet = NSSet(object: employee)
        self.removeRecords(set)
    }
    
    func addRecords(records: NSSet?){
        self.willChangeValueForKey("records", withSetMutation: NSKeyValueSetMutationKind.UnionSetMutation, usingObjects: records)
        self.primitiveValueForKey("records").unionSet(records)
        self.didChangeValueForKey("records", withSetMutation: NSKeyValueSetMutationKind.UnionSetMutation, usingObjects: records)
    }
    
    func removeRecords(records: NSSet?){
        self.willChangeValueForKey("records", withSetMutation: NSKeyValueSetMutationKind.MinusSetMutation, usingObjects: records)
        self.primitiveValueForKey("records").minusSet(records)
        self.didChangeValueForKey("records", withSetMutation: NSKeyValueSetMutationKind.MinusSetMutation, usingObjects: records)
    }
}




class CategoryStore{
    
    class func sharedStore() -> CategoryStore
    {
        struct Static {
            static var instance: CategoryStore?
            static var token: dispatch_once_t = 0
        }
        dispatch_once(&Static.token, {
            Static.instance = CategoryStore()
            })
        return Static.instance!
    }
    
    
    struct Config {
        static let StoreName = "Repsio"
        static let SqliteName = "Repsio.sqlite"
        
        static let StoreOption = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true,NSPersistentStoreUbiquitousContentNameKey:"RepsioCloudStore"]
        

    }
    
    
    
    
    let managedObjectContext: NSManagedObjectContext =
    {
        let moc = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType)
        moc.undoManager = nil
        moc.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        

        return moc
        }()
    
    let managedObjectModel: NSManagedObjectModel =
    {
        let modelURL = NSBundle.mainBundle().URLForResource(Config.StoreName, withExtension: "momd")
        
        let mom = NSManagedObjectModel(contentsOfURL: modelURL)
        
 
        return mom
        }()
    
    
    let persistentStoreCoordinator: NSPersistentStoreCoordinator
    
    init()
    {
       

        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        let urls = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)
        
        let docUrl: NSURL = urls[urls.count - 1] as NSURL
        
        let url = docUrl.URLByAppendingPathComponent(Config.SqliteName)
        
        
        let error: NSErrorPointer = nil
        
        var options:Dictionary = Config.StoreOption
        
        
        if !persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options, error: error) {
            println("\(error)")
        }
        
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        
        
        registiCloudNotification()
    }
    
  
    func registiCloudNotification(){
        
        let StoreWillChangeObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSPersistentStoreCoordinatorStoresWillChangeNotification, object: persistentStoreCoordinator, queue: nil, usingBlock: {(notification: NSNotification?) in
            
            self.managedObjectContext.performBlockAndWait({
                if self.managedObjectContext.hasChanges{
                    
                    var error:NSError?
                    
                    let succeed = self.managedObjectContext.save(&error)
                
                    if error || !succeed{
                        
                        
                    }
                }else{
                    self.managedObjectContext.reset()
                }
                })
            })
        let StoreDidChangeObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSPersistentStoreCoordinatorStoresDidChangeNotification, object: persistentStoreCoordinator, queue: nil, usingBlock: {(notification: NSNotification?) in
            
            
            })
        let ubChangeObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSPersistentStoreDidImportUbiquitousContentChangesNotification, object: persistentStoreCoordinator, queue: nil, usingBlock: {(notification: NSNotification?) in
            
            self.managedObjectContext.performBlockAndWait({
                self.managedObjectContext.mergeChangesFromContextDidSaveNotification(notification)
                })

            })
    }
    
    func createNewCategoryWithName(name: String?) -> Category?
    {
        let obj = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: managedObjectContext) as Category
        
        if name {
            obj.name = name!
        }
        
        return obj
    }
    

    func createNewSessionForCategory(category:Category,andRepeatCount repCount:Int, andSetsCount setsCount:Int) -> Session?
    {
        let obj = NSEntityDescription.insertNewObjectForEntityForName("Session", inManagedObjectContext: managedObjectContext) as Session
        
        obj.timeStamp = NSDate()
        
        obj.repsCount = repCount
        
        obj.category = category
        
        

        for i in 0..setsCount{
            let setinfo = NSEntityDescription.insertNewObjectForEntityForName("SetInfo", inManagedObjectContext: managedObjectContext) as SetInfo
           obj.addSetInfosObject(setinfo)
        }
        

        return obj
    }
    func save() {
        let moc = managedObjectContext
        
        moc.performBlockAndWait {
            let error: NSErrorPointer = nil
            if moc.hasChanges && !moc.save(error) {
                println("\(error)")
            }
        }
    }
    
    func objectWithID(objectID: NSManagedObjectID) -> NSManagedObject {
        return managedObjectContext.objectWithID(objectID)
    }
    
    func deleteObject(object: NSManagedObject) {
        let moc = managedObjectContext
        moc.performBlockAndWait {
            moc.deleteObject(object)
        }
    }
    
    deinit {
        save()
    }
    
}